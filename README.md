# INSTRUCCIONES PARA LEVANTAR EL API REST

## 1. Creación de la base de datos

Para crear la base de datos usuarios debe ejecutar el siguiente comando:

```sh
CREATE DATABASE usuarios_db
    WITH 
    OWNER = postgres
    TEMPLATE = template0
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
```

## 2. Creación de la secuencia

Para crear la secuencia autoincrementable del campo id de la tabla usuarios debe ejecutar el siguiente comando:

```sh
CREATE SEQUENCE usuarios_id_seq
START 1
INCREMENT 1
MINVALUE 1
NO MAXVALUE;
```

## 3. Creación de la tabla usuarios

Para crear la tabla usuarios debe ejecutar el siguiente comando:

```sh
CREATE TABLE usuarios(
	id INTEGER NOT NULL DEFAULT NEXTVAL('usuarios_id_seq'::regclass),
	cedula_identidad VARCHAR(30) NULL,
	nombre VARCHAR(50) NULL,
	primer_apellido VARCHAR(50) NULL,
	segundo_apellido VARCHAR(50) NULL,
	fecha_nacimiento TIMESTAMP NULL,
	PRIMARY KEY(id)
);
```

## 4. Llenado de datos

Para el llenado de datos de la tabla usuarios debe ejecutar los siguientes comandos:

```sh
INSERT INTO usuarios(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento) VALUES('78232355', 'JUAN', 'PEREZ', 'GONZALES', '03-12-1980 00:00:00');
INSERT INTO usuarios(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento) VALUES('48979412', 'RAUL', 'GOMEZ', 'HERNANDEZ', '15-12-1980 00:00:00');
INSERT INTO usuarios(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento) VALUES('15646456', 'ALEX', 'TORREZ', 'JIMENEZ', '05-01-1985 00:00:00');
INSERT INTO usuarios(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento) VALUES('78232355', 'RONALD', 'APARICIO', 'SANCHEZ', '17-05-1984 00:00:00');
INSERT INTO usuarios(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento) VALUES('78232355', 'MARIA', 'ROMERO', 'CALATAYUD', '23-08-1990 00:00:00');
INSERT INTO usuarios(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento) VALUES('78232355', 'CECILIA', 'ALARCON', 'BELTRAN', '12-09-1996 00:00:00');
INSERT INTO usuarios(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento) VALUES('78232355', 'CARMEN', 'FORTUN', 'DAZA', '11-04-1988 00:00:00');
```

## 5. Conexión del API-REST con la Base de Datos

Para configurar los datos de conexión del proyecto con la Base de Datos abrir el archivo index.js del mencionado proyecto y modificar los datos de conexión desde la línea 8 hasta la línea 12:

```sh
const pool = new Pool({
    user: 'oscarsumi',
    host: 'localhost',
    database: 'usuarios_db',
    password: '12345678',
    port: '5432',
});
```

## 6. Ejecución del Proyecto

Para ejecutar el proyecto desde línea de comandos ingresar a la carpeta trabajo-final y ejecutar el siguiente comando

```sh
node index.js
```
