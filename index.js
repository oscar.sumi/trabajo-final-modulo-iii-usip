const express = require('express');
const { Pool } = require('pg');

const app = express();
const port = 4000;

const pool = new Pool({
    user: 'oscarsumi',
    host: 'localhost',
    database: 'usuarios_db',
    password: '12345678',
    port: '5432',
});

// Modelo
class Model{
    async getUsuarios()
    {
        const { rows } = await pool.query('SELECT * FROM usuarios;');
        return rows;
    }

    async getUsuario(id)
    {
        const { rows } = await pool.query('SELECT * FROM usuarios WHERE id = $1;', [id]);
        return rows[0];
    }

    async addUsuario(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento)
    {
        await pool.query('INSERT INTO usuarios(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento) VALUES($1, $2, $3, $4, $5);', [cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento]);
    }

    async updateUsuario(id, cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento)
    {
        await pool.query('UPDATE usuarios SET cedula_identidad = $1, nombre = $2, primer_apellido = $3, segundo_apellido = $4, fecha_nacimiento = $5 WHERE id = $6', [cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento, id]);
    }

    async deleteUsuario(id)
    {
        await pool.query('DELETE FROM usuarios WHERE id = $1', [id]);
    }

    async getPromedioEdad()
    {
        const { rows } = await pool.query('SELECT AVG(EXTRACT(YEAR FROM AGE(NOW(), fecha_nacimiento))) AS promedio_edad FROM usuarios;');
        return rows[0];
    }

    async getEstado()
    {
        const { rows } = await pool.query('SELECT \'api-users\' AS nameSystem, \'0.0.1\' AS version, \'Oscar Aldo Sumi Zamorano\' AS developer, \'oscar.sumi@gmail.com\' AS email;');
        return rows[0];
    }
}

// controlador
class Controller{
    constructor(model) {
        this.model = model;
    }

    async getUsuarios(req, res)
    {
        const data = await this.model.getUsuarios();
        res.send(data);
    }

    async getUsuario(req, res)
    {
        const id = req.params.id;
        const data = await this.model.getUsuario(id);
        res.send(data);
    }

    async addUsuario(req, res)
    {
        const cedula_identidad = req.body.cedula_identidad;
        const nombre = req.body.nombre;
        const primer_apellido = req.body.primer_apellido;
        const segundo_apellido = req.body.segundo_apellido;
        const fecha_nacimiento = req.body.fecha_nacimiento;
        await this.model.addUsuario(cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento);
        res.sendStatus(201);
    }

    async updateUsuario(req, res)
    {
        const id = req.params.id;
        const cedula_identidad = req.body.cedula_identidad;
        const nombre = req.body.nombre;
        const primer_apellido = req.body.primer_apellido;
        const segundo_apellido = req.body.segundo_apellido;
        const fecha_nacimiento = req.body.fecha_nacimiento;
        await this.model.updateUsuario(id, cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento);
        res.sendStatus(200);
    }

    async deleteUsuario(req, res)
    {
        const id = req.params.id;
        await this.model.deleteUsuario(id);
        res.sendStatus(204);
    }

    async getPromedioEdad(req, res)
    {
        const data = await this.model.getPromedioEdad();
        res.send(data);
    }

    async getEstado(req, res)
    {
        const data = await this.model.getEstado();
        res.send(data);
    }
}

const model = new Model();
const controller = new Controller(model);

app.use(express.json());

app.get('/usuarios', controller.getUsuarios.bind(controller));
app.get('/usuarios/:id', controller.getUsuario.bind(controller));
app.post('/usuarios', controller.addUsuario.bind(controller));
app.put('/usuarios/:id', controller.updateUsuario.bind(controller));
app.delete('/usuarios/:id', controller.deleteUsuario.bind(controller));
app.get('/promedio-edad', controller.getPromedioEdad.bind(controller));
app.get('/estado', controller.getEstado.bind(controller));

app.listen(port, () => {
    console.log(`Servidor levantado en http://localhost:${port}`);
});